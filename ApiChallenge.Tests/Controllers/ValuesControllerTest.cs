﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using ApiChallenge.Controllers;
using ApiChallenge.Models.JsonRequest;
using ApiChallenge.Models.JsonResponse;

namespace ApiChallenge.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        private ShowsController showControllerFixture;


        [TestInitialize()]
        public void Initialize()
        {
            this.showControllerFixture = new ShowsController();
        }

        [TestMethod]
        public void ShowsControllerCheckWeCanGetAllTheShowsFromARequest()
        {
 
            JsonRequestRootElement _retrievedShows = new JsonRequestRootElement
            {
                payload = new List<RequestShow> {
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    }
                }
            };


            JsonResponseRootElement response = (JsonResponseRootElement)this.showControllerFixture.Post(_retrievedShows);
            int _showsToShow = 3;

            Assert.AreEqual(response.response.Count(), _showsToShow);
        }

        [TestMethod]
        public void ShowsControllerCheckThatDrmCheckWorks()
        {


            JsonRequestRootElement _retrievedShows = new JsonRequestRootElement { 
                payload = new List<RequestShow> {
                    new RequestShow { 
                        drm = false, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    }
                }
            };


            JsonResponseRootElement response = (JsonResponseRootElement)this.showControllerFixture.Post(_retrievedShows);
            int _showsToShow = 2;

            Assert.AreEqual(response.response.Count(), _showsToShow);
        }

        [TestMethod]
        public void ShowsControllerCheckThatEpisodeCountCheckWorks()
        {

            JsonRequestRootElement _retrievedShows = new JsonRequestRootElement
            {
                payload = new List<RequestShow> {
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=0, 
                        title = "some title", 
                        slug = "some slug", 
                        image = new RequestImage{ showImage= "some image"} 
                    }
                }
            };


            JsonResponseRootElement response = (JsonResponseRootElement)this.showControllerFixture.Post(_retrievedShows);
            int _showsToShow = 2;

            Assert.AreEqual(response.response.Count(), _showsToShow);
        }

        [TestMethod]
        public void ShowsControllerCheckWeCanSendAnArrayContainingNoImages()
        {


            JsonRequestRootElement _retrievedShows = new JsonRequestRootElement
            {
                payload = new List<RequestShow> {
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug"
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug"
                    },
                    new RequestShow { 
                        drm = true, 
                        episodeCount=1, 
                        title = "some title", 
                        slug = "some slug" 
                    }
                }
            };


            JsonResponseRootElement response = (JsonResponseRootElement)this.showControllerFixture.Post(_retrievedShows);
            int _showsToShow = 3;

            Assert.AreEqual(response.response.Count(), _showsToShow);
        }

        [TestMethod]
        public void ShowsControllerCheckResponseIsNullWhenSendingAnEmptyObject()
        {
            JsonResponseRootElement response = (JsonResponseRootElement)this.showControllerFixture.Post(new JsonRequestRootElement());
            
            Assert.AreEqual(response.response, null);
        }


    }
}
