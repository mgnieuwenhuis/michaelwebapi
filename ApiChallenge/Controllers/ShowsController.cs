﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiChallenge.Models.JsonResponse;
using ApiChallenge.Models.JsonRequest;


namespace ApiChallenge.Controllers
{
    public class ShowsController : ApiController
    {
        
        //method to return the episodes list
        public object Post(JsonRequestRootElement _jsonRequestRootElement)
        {
            if (_jsonRequestRootElement != null)
            {
                JsonResponseRootElement _response = _jsonRequestRootElement.getEpisodes();


                return _response;
            }
            else
            {
                HttpError myCustomError = new HttpError() { { "error", "Could not decode request: JSON parsing failed" } };
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, myCustomError);

            }
        }


    }
}