﻿using System.Runtime.Serialization;

namespace ApiChallenge.Models.JsonRequest
{

    public class RequestShow
    {
        
        [DataMember(IsRequired = true)]
        public bool drm { get; set; }

        [DataMember(IsRequired = true)]
        public int episodeCount { get; set; }


        public RequestImage image { get; set; }


        public string slug { get; set; }


        public string title { get; set; }

    }
}

