﻿using System.Collections.Generic;
using System.Linq;
using ApiChallenge.Models.JsonResponse;


namespace ApiChallenge.Models.JsonRequest
{
    public class JsonRequestRootElement
    {
        
        public List<RequestShow> payload { get; set; }

        //retrieves the episodes to show
        public JsonResponseRootElement getEpisodes()
        {
            if (this.payload != null && this.payload.Count() > 0)
            {
                List<RequestShow> _shows = this.payload.Where(e => e.drm == true)
                    .Where(e => e.episodeCount > 0).ToList();
                JsonResponseRootElement _responseJson = new JsonResponseRootElement(_shows);

                return _responseJson;
            }

            return new JsonResponseRootElement();


            

        }
    }
}