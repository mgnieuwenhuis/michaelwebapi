﻿
namespace ApiChallenge.Models.JsonResponse
{
    public class ResponseShow
    {
        public string image;
        public string slug;
        public string title;

    }
}