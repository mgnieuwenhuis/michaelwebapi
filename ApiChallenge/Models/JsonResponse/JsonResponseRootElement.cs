﻿using System.Collections.Generic;
using ApiChallenge.Models.JsonRequest;

namespace ApiChallenge.Models.JsonResponse
{
    public class JsonResponseRootElement
    {
        
        public List<ResponseShow> response {get;set;}

        public JsonResponseRootElement()
        {

        }

        public JsonResponseRootElement(List<RequestShow> _shows)
        {
            response = new List<ResponseShow>();

            
            //go recursively through ArrayList to put them in a new ArrayList object
            foreach (RequestShow show in _shows)
            {

                this.response.Add(new ResponseShow { 
                    image = show.image == null ? "" : show.image.showImage, 
                    slug = show.slug, 
                    title = show.title });
            }
   
        }






    }
}